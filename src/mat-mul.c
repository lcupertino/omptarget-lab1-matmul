
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <omp.h>

#define SIZE 2000

// Initialize matrices.
void init(float *a, float *b, float *c_serial, float *c_parallel) {
  int i, j;
  for (i = 0; i < SIZE; ++i) {
    for (j = 0; j < SIZE; ++j) {
      a[i * SIZE + j] = (float)i + j % 100;
      b[i * SIZE + j] = (float)i + j % 100;
      c_serial[i * SIZE + j] = 0.0f;
      c_parallel[i * SIZE + j] = 0.0f;
    }
  }
}

/// matrix multiplication algorithm
void mul_serial(float *a, float *b, float *c) {
    int i, j, k;
    float sum = 0.0;

    for (i = 0; i < SIZE; ++i) {
      for (j = 0; j < SIZE; ++j) {
        sum = 0.0;
        for (k = 0; k < SIZE; ++k) {
          sum = sum + a[i * SIZE + k] * b[k * SIZE + j];
        }
        c[i * SIZE + j] = sum;
      }
    }
}

// TODO: Parallelize the function
void mul_parallel(float *a, float *b, float *c) {

  int i, j, k;
  float sum = 0.0;

  for (i = 0; i < SIZE; ++i) {
    for (j = 0; j < SIZE; ++j) {
      sum = 0.0;
      for (k = 0; k < SIZE; ++k) {
        sum = sum + a[i * SIZE + k] * b[k * SIZE + j];
      }
      c[i * SIZE + j] = sum;
    }
  }
}

int compareResults(float *b_serial, float *b_parallel) {
  int i, j, fail;
  fail = 0;

  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      if (b_serial[i * SIZE + j] != b_parallel[i * SIZE + j]) {
        fail++;
        if (i < 10)
          fprintf(stdout, "%f != %f \n", b_serial[i * SIZE + j],
                  b_parallel[i * SIZE + j]);
      }
    }
  }

  // Print results
  printf("Non-Matching Parallel-Serial Outputs: %d\n", fail);
  return fail;
}

int main(int argc, char *argv[]) {

  double t_start, t_end;
  int fail = 0;
  float *a, *b, *c_serial, *c_parallel;

  a = (float *)malloc(sizeof(float) * SIZE * SIZE);
  b = (float *)malloc(sizeof(float) * SIZE * SIZE);
  c_serial = (float *)calloc(sizeof(float), SIZE * SIZE);
  c_parallel = (float *)calloc(sizeof(float), SIZE * SIZE);

  init(a, b, c_serial, c_parallel);

  fprintf(stdout, "<< Matrix Multiplication >>\n");

  t_start = omp_get_wtime();
  mul_serial(a, b, c_parallel);
  t_end = omp_get_wtime();
  fprintf(stdout, "Serial Runtime: %0.6lfs\n", t_end - t_start);

  t_start = omp_get_wtime();
  mul_parallel(a, b, c_serial);
  t_end = omp_get_wtime();
  fprintf(stdout, "Parallel Runtime: %0.6lfs\n", t_end - t_start);

  fail = compareResults(c_serial, c_parallel);

  free(a);
  free(b);
  free(c_serial);
  free(c_parallel);

  return fail;
}
